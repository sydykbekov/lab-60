import React, {Component} from 'react';
import './App.css';
import Design from './components/Design/Design';

class App extends Component {
    state = {
        author: '',
        message: '',
        lastDate: ''
    };

    appendPost = (obj) => {
        let div = document.createElement('div');
        div.setAttribute('class', 'post');
        let messageText = document.createElement('p');
        messageText.innerHTML = obj.message;
        let author = document.createElement('span');
        author.setAttribute('class', 'authors');
        author.innerHTML = obj.author;
        let date = document.createElement('span');
        date.setAttribute('class', 'date');
        date.innerHTML = obj.datetime;
        div.appendChild(author);
        div.appendChild(messageText);
        div.appendChild(date);
        let posts = document.getElementById('posts');
        posts.appendChild(div);
    };

    appendWhileHaveNewPosts = (response) => {
        for (let i = 0; i < response.length; i++) {
            this.appendPost(response[i]);
        }
        let lastDate = response[response.length - 1].datetime;
        this.setState({lastDate});
    };

    authorName = (event) => {
        this.setState({author: event.target.value});
    };

    authorMessage = (event) => {
        this.setState({message: event.target.value});
    };

    sendMessage = () => {
        let obj = {
            author: this.state.author,
            message: this.state.message
        };

        let data = new FormData();
        data.append("json", JSON.stringify(obj));

        fetch("http://146.185.154.90:8000/messages",
            {
                method: "POST",
                body: data
            })
            .then(res => res.json())
            .then(data => alert(JSON.stringify(data)));
    };

    componentDidMount() {
        fetch('http://146.185.154.90:8000/messages').then(response => {
            return response.json();
        }).then(response => {
            this.appendWhileHaveNewPosts(response);
            setInterval(() => {
                console.log('==================');
                fetch('http://146.185.154.90:8000/messages?datetime=' + this.state.lastDate).then(response => {
                    return response.json();
                }).then(response => {
                    if (response.length !== 0) {
                        this.appendWhileHaveNewPosts(response);
                    }
                });
            }, 2000);
        });
    };

    render() {
        return (
            <div className="App">
                <Design author={this.authorName} message={this.authorMessage} sending={this.sendMessage}/>
            </div>
        );
    }
}

export default App;
