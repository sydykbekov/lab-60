import React from 'react';
import './Design.css';

const Design = props => {
    return(
        <div id="container">
            <div id="logo">Messenger</div>
            <div id="posts" />
            <div id="footer">
                <input onChange={props.author} id="author" type="text" />
                <textarea onChange={props.message} placeholder="Text" name="" id="message" cols="50" rows="4" />
                <button onClick={props.sending} id="send">Send</button>
            </div>
        </div>

    )
};



export default Design;